# OPZ VideoPak Mushroom Forest

This is my first VideoPak built for the OPZ App ! 

Developped in Unity 18.04 with the VideoLab 1.1.1 Unity Pack. 

# Licence

Creative Common CC-BY-SA

# Credits where it's due 

Thanks to [Lisa Padilla](https://poly.google.com/user/5JNpEcmvcpX) for the [wonderful magical forest](https://poly.google.com/view/4MmQ8g7OU6f) ! 

And to [Vladimir Ilic](https://poly.google.com/user/eW-UZX3VB0K) for the crazy looking [Owl](https://poly.google.com/view/6kuj9BdaDgA). 

And thanks to Keijiro for the[ examples he provided ](https://github.com/keijiro/VideolabTest) which helped me kick start this !